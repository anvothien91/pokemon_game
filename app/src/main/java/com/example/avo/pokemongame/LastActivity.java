package com.example.avo.pokemongame;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class LastActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_last);
        Bundle bundle = getIntent().getExtras();
        String status = bundle.getString("Status");
        String score = bundle.getString("Score");
        final TextView endTextView = (TextView) findViewById(R.id.endTextView);
        final TextView scoreTextView = (TextView) findViewById(R.id.endScoreView);

        if(status.equals("won")){
            endTextView.setText("Congratulations!  You win!");
        }

        if(status.equals("lost")){
            endTextView.setText("Game over.  You lose!");
        }

        scoreTextView.setText(score);

        Button mainMenuButton = (Button) findViewById(R.id.mainMenuButton);
        mainMenuButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                Intent intent = new Intent(view.getContext(), MainActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onBackPressed(){

    }
}

