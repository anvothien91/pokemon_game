package com.example.avo.pokemongame;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.EmptyStackException;
import java.util.List;

import game_client.Game;
import generations.IGetGeneration;


public class GameActivity extends AppCompatActivity {

    private MediaPlayer mediaPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        Bundle regionBundle = getIntent().getExtras();

        final Game game = new Game(regionBundle);
        final ImageView pokemonView = (ImageView) findViewById(R.id.pokemonView);
        final TextView animationTextView = (TextView) findViewById(R.id.animationView);
        Button button = (Button) findViewById(R.id.submitButton);
        final View scoreView = findViewById(R.id.scoreView);
        final View lifeView = findViewById(R.id.lifeView);
        final AutoCompleteTextView inputText = (AutoCompleteTextView) findViewById(R.id.autoCompleteInput);

        mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.battle);
        mediaPlayer.setLooping(true);
        mediaPlayer.start();

        List<IGetGeneration> regions = game.getRegions();
        String[] autoCompleteValues = game.getAutoCompletePokemonValues(regions);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.select_dialog_item, autoCompleteValues);
        final AutoCompleteTextView autoCompleteInputView = (AutoCompleteTextView) findViewById(R.id.autoCompleteInput);
        autoCompleteInputView.setThreshold(1);
        autoCompleteInputView.setAdapter(adapter);
        autoCompleteInputView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                in.hideSoftInputFromWindow(view.getApplicationWindowToken(),0);
            }
        });

        pokemonView.setImageResource(game.GetCurrentGuessId());
        UpdateCurrentScoreView(scoreView, game);
        UpdateCurrentPlayerLivesView(lifeView, game);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    if(game.IsCorrectAnswer(inputText.getText().toString())){
                        game.SetScore(game.GetScore() + 10);
                        startFadeOutAnimation(animationTextView, "Correct!");
                        UpdateCurrentScoreView(scoreView,game);
                    }
                    else {
                        game.SetNumOfLives(game.GetNumOfLives() - 1);
                        UpdateCurrentPlayerLivesView(lifeView, game);
                        startFadeOutAnimation(animationTextView, "Incorrect!");
                    }

                    if(game.IsPlayerOutOfLives()){
                        //Game over.  Go to Last Activity
                        Bundle gameOverBundle = new Bundle();
                        gameOverBundle.putString("Status", "lost");
                        gameOverBundle.putString("Score",  String.valueOf(game.GetScore()));
                        Intent intent = new Intent(view.getContext(), LastActivity.class);
                        intent.putExtras(gameOverBundle);
                        startActivity(intent);
                    }

                    game.SetNewCurrentGuessId();
                    pokemonView.setImageResource(game.GetCurrentGuessId());

                    if(game.playerIsNearTheEnd() && game.getEndingSongPlayed() == false)
                    {
                        if(mediaPlayer != null) {
                            mediaPlayer.stop();
                            mediaPlayer.release();
                            mediaPlayer = null;
                            mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.gym_battle);
                            mediaPlayer.setLooping(true);
                            mediaPlayer.start();
                            game.setEndingSongPlayed(true);
                        }
                    }

                }
                catch(EmptyStackException e){
                    //Game is over.  Player has exhausted all available pokemon to guess
                    Bundle winningBundle = new Bundle();
                    winningBundle.putString("Status", "won");
                    winningBundle.putString("Score", String.valueOf(game.GetScore()));
                    Intent intent = new Intent(view.getContext(), LastActivity.class);
                    intent.putExtras(winningBundle);
                    startActivity(intent);
                }

                finally{
                    autoCompleteInputView.setText("");
                }
            }
        });
    }

    @Override
    public void onBackPressed(){

    }

    @Override
    protected void onPause(){
        super.onPause();
        mediaPlayer.pause();
    }

    @Override
    protected void onResume(){
        super.onResume();
        mediaPlayer.seekTo(mediaPlayer.getCurrentPosition());
        mediaPlayer.start();
    }

    protected void UpdateCurrentScoreView(View view, Game game){
        TextView scoreView = (TextView) findViewById(R.id.scoreView);
        scoreView.setText("Score: " + game.GetScore());
    }

    protected void UpdateCurrentPlayerLivesView(View view, Game game) {
        TextView playerLifeView = (TextView) findViewById(R.id.lifeView);
        playerLifeView.setText("Lives: " + game.GetNumOfLives());
    }

    public void startFadeOutAnimation(View view, String message){
        final TextView animationView = (TextView) findViewById(R.id.animationView);
        Animation fadeOutAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_out);
        animationView.setText(message);
        animationView.setTextSize(25);
        animationView.startAnimation(fadeOutAnimation);
        fadeOutAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }
            @Override
            public void onAnimationEnd(Animation animation) {
                animationView.setVisibility(View.GONE);
            }
            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }
}
