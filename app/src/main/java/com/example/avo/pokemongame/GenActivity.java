package com.example.avo.pokemongame;

import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Switch;

import java.util.ArrayList;
import java.util.List;

import game_client.ErrorDialogFragment;

public class GenActivity extends AppCompatActivity {
    private Switch kantoSwitch;
    private Switch johtoSwitch;
    private Switch hoennSwitch;
    private Switch sinnohSwitch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gen);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        kantoSwitch = (Switch) findViewById(R.id.kantoSwitch);
        johtoSwitch = (Switch) findViewById(R.id.johtoSwitch);
        hoennSwitch = (Switch) findViewById(R.id.hoennSwitch);
        sinnohSwitch = (Switch) findViewById(R.id.sinnohSwitch);
    }

    public void startGame(View view) {
        Bundle regionBundle = new Bundle();
        if(checkSwitches()){
            SetRegionValues(regionBundle);
            Intent intent = new Intent(this, GameActivity.class);
            intent.putExtras(regionBundle);
            startActivity(intent);
        }
        else{
            FragmentManager fm = getFragmentManager();
            ErrorDialogFragment errorFragment = new ErrorDialogFragment();
            errorFragment.show(fm, "yolo");
        }
    }

    public boolean checkSwitches(){
        return (kantoSwitch.isChecked() || johtoSwitch.isChecked() || hoennSwitch.isChecked() || sinnohSwitch.isChecked());
    }

    private void SetRegionValues(Bundle regionBundle) {
            if(kantoSwitch.isChecked()){
                regionBundle.putString("Kanto", "true");
            }
            else {
                regionBundle.putString("Kanto", "false");
            }
            if(johtoSwitch.isChecked()){
                regionBundle.putString("Johto", "true");
            }
            else {
                regionBundle.putString("Johto", "false");
            }
            if(hoennSwitch.isChecked()){
                regionBundle.putString("Hoenn", "true");
            }
            else{
                regionBundle.putString("Hoenn", "false");
            }
            if(sinnohSwitch.isChecked()){
                regionBundle.putString("Sinnoh", "true");
            }
            else{
                regionBundle.putString("Sinnoh", "false");
            }
    }
}
