package game_client;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.Collections;
import java.util.EmptyStackException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import generations.Hoenn;
import generations.IGetGeneration;
import generations.Johto;
import generations.Kanto;
import generations.Sinnoh;

public class Game {

    private int numOfLives;
    private int playerScore;
    private Map<Integer, String> availablePokemon;
    private Stack guessKeys;
    private Integer currentGuessId;
    private Integer startingStackCount;
    private boolean endingSongPlayed;


    public Game(Bundle bundle) {
        numOfLives = 5;
        playerScore = 0;
        availablePokemon = GetAllAvailablePokemonToGuess(bundle);
        guessKeys = new Stack();
        SetGuessKeys();
        currentGuessId = (Integer) guessKeys.peek();
        startingStackCount = guessKeys.size();
        endingSongPlayed = false;
    }

    public boolean playerIsNearTheEnd(){
        int currentStackCount = guessKeys.size();
        double percentage = (double) (startingStackCount - currentStackCount) / startingStackCount * 100;
        if(percentage > 75)
            return true;
        return false;
    }

    public boolean getEndingSongPlayed(){
        return endingSongPlayed;
    }

    public void setEndingSongPlayed(boolean endingSongPlayed){
        this.endingSongPlayed = endingSongPlayed;
    }

    public String[] getAutoCompletePokemonValues(List<IGetGeneration> generations){

        HashSet<String> autoCompleteValues = new HashSet();
        for (IGetGeneration item: generations) {
            Map<Integer,String> gen_map  = item.GetGeneration();
            for(Map.Entry<Integer,String> mapEntry : gen_map.entrySet()){
                autoCompleteValues.add(mapEntry.getValue().toLowerCase());
            }
        }
        return autoCompleteValues.toArray(new String[autoCompleteValues.size()]);
    }

    public List<IGetGeneration> getRegions(){
        List<IGetGeneration> regions = new ArrayList<>();
        regions.add(new Kanto());
        regions.add(new Johto());
        regions.add(new Hoenn());
        regions.add(new Sinnoh());
        return regions;
    }

    public int GetScore(){
        return playerScore;
    }

    public boolean IsPlayerOutOfLives(){
        return (GetNumOfLives() == 0);
    }

    public void SetScore(int playerScore){
        this.playerScore = playerScore;
    }

    public int GetNumOfLives(){
        return numOfLives;
    }

    public void SetNumOfLives(int numOfLives){
        this.numOfLives = numOfLives;
    }

    public Integer SetNewCurrentGuessId(){
        if(!guessKeys.isEmpty()) {
            guessKeys.pop();
            currentGuessId = (Integer) guessKeys.peek();
            return currentGuessId;
        }
        else{
            throw new EmptyStackException();
        }
    }

    public Integer GetCurrentGuessId(){
        return currentGuessId;
    }

    public boolean IsCorrectAnswer(String playerInput){
        Integer currentAnswerGuessId = GetCurrentGuessId();
        String correctAnswer = availablePokemon.get(currentAnswerGuessId);
        return(playerInput.toLowerCase().equals(correctAnswer.toLowerCase()));
    }

    public void SetGuessKeys(){
        List<Map.Entry<Integer,String>> list = new ArrayList<>(availablePokemon.entrySet());
        Collections.shuffle(list);
        for(Map.Entry<Integer, String> entry: list){
             guessKeys.push(new Integer(entry.getKey()));
        }
    }

    public Map<Integer, String> GetAllAvailablePokemonToGuess(Bundle regionBundle){
        Map<Integer,String> availablePokemon = new HashMap<>();
        String kantoValue = regionBundle.getString("Kanto");
        String johtoValue = regionBundle.getString("Johto");
        String hoennValue = regionBundle.getString("Hoenn");
        String sinnohValue = regionBundle.getString("Sinnoh");

        if(kantoValue.equals("true")) {
            Kanto kantoRegion = new Kanto();
            availablePokemon.putAll(kantoRegion.GetGeneration());
        }
        if(johtoValue.equals("true")){
            Johto johtoRegion = new Johto();
            availablePokemon.putAll(johtoRegion.GetGeneration());
        }

        if(hoennValue.equals("true")){
            Hoenn hoennRegion = new Hoenn();
            availablePokemon.putAll(hoennRegion.GetGeneration());
        }

        if(sinnohValue.equals("true")){
            Sinnoh sinnohRegion = new Sinnoh();
            availablePokemon.putAll(sinnohRegion.GetGeneration());
        }
        return availablePokemon;
    }
}
