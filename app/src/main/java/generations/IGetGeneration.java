package generations;

import java.util.List;
import java.util.Map;

public interface IGetGeneration
{
    Map<Integer,String> GetGeneration();
}
