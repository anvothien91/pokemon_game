package generations;

public class GenerationFactory
{
    public Generation GetGenerationModel(String generation)
    {
        switch(generation.toLowerCase())
        {
            case "kanto":
                return new Kanto();
            default:
                return null;
        }
    }
}
